#!/usr/bin/env python

import json
import os
from collections import OrderedDict

# Global config
CONFIG    = "./config.json"
with open(CONFIG) as cf:
    config = json.load(cf)

# Nodes config directory
NODES_DIR = "./nodes"

def main():
    # Load node definitions
    nodes = []
    for fn in os.listdir(NODES_DIR):
        with open(os.path.join(NODES_DIR, fn)) as node_file:
            for node in json.load(node_file):
                nodes.append(node)

    devices = gen_devices(nodes)
    write_header(devices)

def gen_devices(nodes):
    """Returns dictionary containing devices.h header definitions"""

    # Dictionary representing name:value pairs to write to header as #defines
    header = OrderedDict()

    for node in nodes:
        # Get node-specific prefix
        scandal_prefix = node["scandal_prefix"]

        # Define node address
        header[scandal_prefix] = str(node["address"])

        # Define number of in/out channels
        scandal_out_ch = scandal_prefix + "_" + config["scandal_num_out_channels"]
        scandal_in_ch  = scandal_prefix + "_" + config["scandal_num_in_channels"]
        header[scandal_out_ch] = str(len(node["output_channels"]))
        header[scandal_in_ch]  = str(len(node["input_channels"]))

        # Define in/out channel addresses
        for ch in node["output_channels"] + node["input_channels"]:
            scandal_name = node["scandal_prefix"] + "_" + ch["scandal_id"]
            channel = str(ch["channel"])
            header[scandal_name] = channel

    return header

def write_header(d):
    """Generates full header file"""

    with open(config["output"], 'w') as f:
        with open(config["disclaimer"]) as disclaimer:
            f.write(disclaimer.read())
        for name, value in d.items():
            f.write("#define %s %s\n" % (name, value))

if __name__ == "__main__":
    main()
