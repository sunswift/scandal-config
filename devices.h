// ===========================================
// Scandal node address and device definitions
// ===========================================

// THIS IS AN AUTOMATICALLY GENERATED FILE!
// Please do not modify this file directly.
// Use the scandal-config generator to update device definitions.

#define CURRENTINT 51
#define CURRENTINT_NUM_OUT_CHANNELS 6
#define CURRENTINT_NUM_IN_CHANNELS 1
#define CURRENTINT_CURRENT 0
#define CURRENTINT_VOLTAGE 1
#define CURRENTINT_POWER 2
#define CURRENTINT_CURRENTINT 3
#define CURRENTINT_POWERINT 4
#define CURRENTINT_SAMPLES 5
#define CURRENTINT_RESET_INTEGRATION 0
#define GPS 70
#define GPS_NUM_OUT_CHANNELS 6
#define GPS_NUM_IN_CHANNELS 0
#define GPS_TIME 0
#define GPS_LATITUDE 1
#define GPS_LONGITUDE 2
#define GPS_ALTITUDE 3
#define GPS_SPEED 4
#define GPS_ODOMETER 5
#define SMARTDCDC 70
#define SMARTDCDC_NUM_OUT_CHANNELS 10
#define SMARTDCDC_NUM_IN_CHANNELS 0
#define SMARTDCDC_BATT_VOLTAGE 0
#define SMARTDCDC_WAVESCULPTOR_VOLTAGE 1
#define SMARTDCDC_5V_VOLTAGE 2
#define SMARTDCDC_12V_VOLTAGE 3
#define SMARTDCDC_CAN_5V_VOLTAGE 4
#define SMARTDCDC_CAN_12V_VOLTAGE 5
#define SMARTDCDC_IN_CURRENT 6
#define SMARTDCDC_OUT_CURRENT 7
#define SMARTDCDC_MSP_TEMP 8
#define SMARTDCDC_HEATSINK_TEMP 9
